# Swissclinic_CaptchaFix
Contains a fix for a problem in MSP_Recaptcha. Can be uninstalled when MSP_ReCaptcha version 2.1.3 or 2.2.0 comes out of pre-release.